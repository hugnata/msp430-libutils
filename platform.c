#include "msp430.h"
#include "platform.h"

void platform_init() {
    // Disable watchdog timer
    WDTCTL = WDTPW | WDTHOLD;
    // Set the clock to 16MHz
    CSCTL0_H = CSKEY >> 8;
    CSCTL1 = DCOFSEL_4 | DCORSEL;
    // Unlock GPIO pins
    PM5CTL0 &= ~LOCKLPM5;
}

void disable_io()
{

    PAOUT = 0x00;
    PASEL0 = 0x00;
    PASEL1 = 0x00;
    PADIR = 0xFF;
    PAREN = 0x00;
    PAIE = 0x00;

    PBOUT = 0x00;
    PBSEL0 = 0x00;
    PBSEL1 = 0x00;
    PBDIR = 0xFF;
    PBREN = 0x00;

    PBIE = 0x00;
    PJOUT = 0x00;
    PJSEL0 = 0x00;
    PJSEL1 = 0x00;
    PJDIR = 0xFF;
    PJREN = 0x00;
}
