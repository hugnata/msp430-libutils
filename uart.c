/*
 * uart.c
 *
 * Copyright (C)
 * 		https://github.com/derf/msp430fr5969-playground
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the MIT License
 */

#include <stdarg.h>
#include "uart.h"

static int uart_initialized = 0;

typedef unsigned short int uint16_t;

int printf(const char *format, ...)
{
    if (!uart_initialized)
    {
        uart_setup();
    }

    va_list args;
    va_start(args, format);

    while (*format != '\0')
    {
        if (*format != '%')
        {
            uart_putchar(*format);
        }
        else
        {
            // Handle format specifiers
            switch (*++format)
            {
            case 'd':
                uart_putint(va_arg(args, int));
                break;
            case 'x':
            case 'X':
                uart_puthex(va_arg(args, unsigned int));
                break;
            case 's':
                uart_puts(va_arg(args, char *));
                break;
            case 'f':
                // count += fprintf(stdout, "%f", va_arg(args, double));
                break;
            default:
                // Handle unknown format specifiers
                uart_putchar('%');
                uart_putchar(*format);
                break;
            }
        }
        format++;
    }

    va_end(args);
    return 0;
}


int putchar(int c) {
    if (!uart_initialized)
    {
        uart_setup();
    }
    uart_putchar(c);
    return 0;
}

void uart_putchar(char c)
{
    while (!(UCA0IFG & UCTXIFG))
        ;
    UCA0TXBUF = c;

    if (c == '\n')
        uart_putchar('\r');
}

void uart_putdigit(unsigned char digit)
{
    if (digit < 10)
        uart_putchar('0' + digit);
    else
        uart_putchar('A' + digit - 10);
}

void uart_puthex(unsigned char num)
{

    uart_putdigit(num >> 4);
    uart_putdigit(num & 0x0f);
}

void uart_putint(int num)
{
    if (num > 10000)
        uart_putdigit(num / 10000);

    if (num > 1000)
        uart_putdigit((num % 10000) / 1000);

    if (num > 100)
        uart_putdigit((num % 1000) / 100);

    if (num > 10)
        uart_putdigit((num % 100) / 10);

    uart_putdigit(num % 10);
}

void uart_putfloat(float num)
{
    if (num > 1000)
        uart_putdigit(((int)num % 10000) / 1000);
    if (num > 100)
        uart_putdigit(((int)num % 1000) / 100);
    if (num > 10)
        uart_putdigit(((int)num % 100) / 10);

    uart_putdigit((int)num % 10);
    uart_putchar('.');
    uart_putdigit((int)(num * 10) % 10);
    uart_putdigit((int)(num * 100) % 10);
}

int puts(char *text)
{
    if (!uart_initialized)
    {
        uart_setup();
    }
    uart_puts(text);
    uart_putchar('\n');
    return 0;
}

void uart_puts(char *text)
{
    while (*text)
        uart_putchar(*text++);
}

void uart_puterr(char *text)
{
    uart_puts(COL_RED);
    uart_puts(text);
    uart_puts(COL_RESET);
}

void uart_nputs(char *text, int len)
{
    int i;
    for (i = 0; i < len; i++)
        uart_putchar(text[i]);
}

uint16_t getDCOConfig()
{
    const uint16_t dcorsel = (CSCTL1 & DCORSEL) >> 6; /* bit 0 */
    const uint16_t dcofsel = (CSCTL1 & 0xE);          /* bit 3..1 */
    return dcofsel | dcorsel;
}

/** these 2 tabulars are generated from the serial.py script
 *  that off-line compute the register values to configure
 *  UART timings (depending on both the input frequency (dco)
 *  and the uart frequency (9600 bauds)
 */
const uint16_t tpl_brwTab[] = {
    0x6, 0x6, 0x11, 0x22, 0x16, 0x2d, 0x1a, 0x34,
    0x22, 0x68, 0x2d, 0x88, 0x34, 0x9c, 0x0, 0x0};
const uint16_t tpl_mctlwTab[] = {
    0x2081, 0x2081, 0x861, 0x11b1, 0x6bc1, 0x2091, 0xb601, 0x2511,
    0x11b1, 0xb621, 0x2091, 0x55b1, 0x2511, 0x41, 0x0, 0x0};

void uart_setup(void)
{
    //    UCA0CTLW0 = UCSWRST | UCSSEL__SMCLK;
    //    UCA0MCTLW = UCOS16 | (2<<5) | 0xD600;
    //    UCA0BR0 = 104;
    //
    //    UCA0IRCTL = 0;
    //    UCA0ABCTL = 0;
    //
    //    P2SEL0 &= ~(BIT0 | BIT1);
    //    P2SEL1 |= BIT0 | BIT1;
    //    P2DIR |= BIT0;
    //
    //    UCA0CTLW0 &= ~UCSWRST;
    //
    //    UCA0IE |= UCRXIE;

    CSCTL0 = CSKEY;         /* Get access to CSCTLx regs */
    CSCTL2 &= ~SELS_7;      /* Set DCO as SMCLK (clear field..      */
    CSCTL2 |= SELS__DCOCLK; /* .. and set correct DCO value)        */
    /* then remove the DIVS division */
    CSCTL3 &= ~(DIVS0_L | DIVS1_L | DIVS2_L);
    /* set IO: fonction 10 for P2.0 (Tx) and P2.1 (Rx) */
    P2SEL0 &= ~0x03;
    P2SEL1 |= 0x03;

    uint16_t dcoConfig = getDCOConfig();
    /* set Software reset (and reinit register) */
    UCA0CTLW0 = UCSWRST;
    /* default 8N1, lsb first, uart, input clk SMCLK */
    UCA0CTLW0 |= UCSSEL__SMCLK;
    /* baud rates are pre-calculated for 9600. */
    UCA0BRW = tpl_brwTab[dcoConfig];
    UCA0MCTLW = tpl_mctlwTab[dcoConfig];
    /* get out of reset state */
    UCA0CTLW0 &= ~UCSWRST;
    uart_initialized = 1;
}
