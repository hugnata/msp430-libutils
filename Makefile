TARGET=libmsp430utils.a
GCC=msp430-elf-gcc
AR=msp430-elf-ar

# Available flags:
# -DADC_DEBUG Raise a given pin when the ADC is being read, or if we are sleeping, waiting for the voltage to go up
# -DCHECKPOINT_DEBUG Raise a given pin when a checkpoint is being saved
#  Enable the no re-execution policy
# -DMEMENTOS_SKIP Checkpoint are skip when the startup voltage is not reached
# -DACTIVE_SAMPLING Instead of relying on ADC interrupt to wake up, poll the voltage across the ADC

FLAGS+=
COPT=-Wall -g3 -mmcu=msp430fr5969 -DCHECKPOINT_DEBUG -DADC_DEBUG

OBJS=adc.o mementos.o mementos_fram.o uart.o platform.o

all: $(OBJS)
	$(AR) rcs $(TARGET) $(OBJS)

%.o: %.c
	$(GCC) $(COPT) $(FLAGS) -c $<

clean:
	rm -f *.o *.a

examples:
