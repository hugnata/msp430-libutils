/*
 * adc.h
 *
 * Copyright (C)
 * 		2023 Hugo Reymond
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the MIT License
 */

#ifndef ADC_H_
#define ADC_H_

/** Initialize the ADC
 *
 * The settings are the following:
 * - The channel VBATMAP is selected (Vin=AVCC/2)
 * - The ADC takes the 1.2V internal reference voltage as V+
 */
void adc_init();

/** Measure the voltage
 *
 * This function tries first to use the 1.2V internal reference to measure the voltage.
 * If the voltage is too high, it will switch to the 2V internal reference.
 *
 * The measured voltage is converted to mV using the following formula:
 * - If the 2V reference is used, the return value is Vreal * 1.024
 * - If the 1.2V reference is used, the return value is Vreal * 0.9964
 *
 * @return The voltage measured in approximated mV
 */
int adc_measure();

/**
 * @brief Put the MSP430 in LPM3 until the voltage is above the given value
 *
 * Rely on the Timer_A to wake up the MSP430
 *
 * @param value The voltage to reach in mV
 */
void sleep_until_voltage_reached(int value);
// void adc_shutdown();



#endif /* ADC_H_ */
