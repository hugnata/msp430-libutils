/*
 * adc.c
 *
 * Copyright (C)
 * 		2023 Hugo Reymond
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the MIT License
 */
#include "adc.h"
#include "msp430.h"

// Only used when in ACTIVE_SAMPLING mode
// Define the start value for the timer. This value impacts the sleep duration of the MSP430
// between two voltage measurements. The time between two measurements is 0xFFFF - TIMER_INIT,
// in clock cycles
#define TIMER_INIT 0xF000

// Only used when ACTIVE_SAMPLING mode is disabled
// ADC SAMPLE PERIOD, in ms.
#define ADC_SAMPLE_PERIOD 4
#define CCR0RESETVAL ADC_SAMPLE_PERIOD * 10

// The pin that will be used to signal when sleeping until voltage reaches custom threshold
#define MEASURE BIT4
static int adc_init_done = 0;

void adc_init()
{
    while (REFCTL0 & REFGENBUSY)
        ;
    // Set Internal reference voltage to 1.2V
    REFCTL0 = REFVSEL_0 | REFON;
    // Make sure the ENC bit is cleared before initializing the ADC12
    ADC12CTL0_L &= ~ADC12ENC;
    // Turn OFF ADC12B Module
    ADC12CTL0 &= ~(ADC12ON + ADC12ENC + ADC12SC);

    // Set ADC settings
    ADC12CTL0 = ADC12SHT0_2 | ADC12ON; // Sample and Hold time=16ADC12CLK time, ADC12 on
    ADC12CTL1 |= ADC12SHP;             // Set the sampling signal clock source to the sampling timer
    ADC12CTL2 |= ADC12RES_2;           // Set resolution to 12bit (disable 12bit first !)

    // Select voltage references: VR+=VREF, VR-=AVSS
    // Set ADC input channel to Channel A30
    ADC12MCTL0 = ADC12VRSEL_1 + ADC12INCH_31;

    // Link the ADC channel A30 to the AVCC/2 (why ? it has already been setup
    ADC12CTL3 |= ADC12BATMAP;

#ifdef ADC_DEBUG
    P1DIR |= MEASURE;
#endif

    adc_init_done = 1;
}

inline int adc_measure()
{
    // This function initializes the ADC if necessary, then measures the voltage
    // using the 1.2V reference voltage. If the ADC saturates (Vin > 2.4V), it
    // switches to the 2V reference voltage and measures again.
    //
    // This function returns the voltage in mV

    if (!adc_init_done)
    {
        adc_init();
    }

    ADC12CTL0 |= ADC12ON; // Turn on ADC

    // Set internal reference to 1.2V
    while (REFCTL0 & REFGENBUSY)
        ;
    REFCTL0 = REFVSEL_0 | REFON;
    while (!(REFCTL0 & REFGENRDY))
        ;

    // Start conversion
    ADC12CTL0 |= ADC12ENC + ADC12SC;
    // Wait until the conversion is done (@TODO: go to LPM0 )
    while (ADC12CTL1 & ADC12BUSY)
        ;
    int value = ADC12MEM0;
    // ADC12IER0 &= ~ADC12IER0;

    // Check if we reached ADC saturation (i.e Vin > 1.2V, so VCC > 2.4V)
    if (value > 4040)
    {
        // Switch to 2V reference voltage
        while (REFCTL0 & REFGENBUSY)
            ;
        REFCTL0 = REFVSEL_1 | REFON; // Set internal reference to 2V
        while (!(REFCTL0 & REFGENRDY))
            ;

        // Perform measurement
        ADC12CTL0 |= ADC12ENC + ADC12SC;
        while (ADC12CTL1 & ADC12BUSY)
            ;

        value = ADC12MEM0;
    }
    else
    {
        // If VCC is within the 2.4V range, divide the ADC value
        // by 1.7 to convert it to mV (multiply by 0.589)
        value = value >> 1; // Multiply by 0.5
        int tmp = value >> 3;
        value += tmp; // Add 0.062
        tmp = tmp >> 2;
        value += tmp; // Add 0.035 (0.562>>4)
        tmp = tmp >> 2;
        value -= tmp; // Remove 0.008 (0.562>>6)
    }
    ADC12CTL0 &= ~ADC12ENC; // disable any conversion to allow ADC configuration
    ADC12CTL0 &= ~ADC12ON;  // Turn off ADC

    return value;
}

// #define ACTIVE_SAMPLING

#ifndef ACTIVE_SAMPLING

/** Go to LPM3 until the VCC voltage is above the chosen voltage
 *
 * The function first samples the ADC to know if we are above the given voltage.
 * If not, it activate the ADC and set its trigger to Timer A CCR1 signal.
 * The system then goes to sleep, and the adc regularly measure (given the ADC_SAMPLE_PERIOD defined in adc.c)
 * the current VCC voltage to see if it exceeds the threshold
 *
 * /!\ Warnings /!\
 * The function won't exactly wake up at `voltage` but at `voltage` * 0,976.
 * This is due to the ADC resolution being on 12bit , and values going from 0 to 4096 instead of 0 to 4000.
 * If you want the system to wake up a 3V, you must set the input voltage to 3072.
 *
 * If the voltage <= 0, the function consider an error occured and wait until the voltage reaches 3000mV
 * If the voltage <= 1800, the function returns as if the msp is on, it means that VCC > 1.8V
*/
void sleep_until_voltage_reached(int voltage)
{
#ifdef ADC_DEBUG
    P1DIR |= MEASURE;
    P1OUT |= MEASURE;
#endif
    if (voltage <= 0)
    {
        // An error might have occured ! Set Vwake_up to 3000
        voltage = 3000;
    }

    // If wake-up voltage is < 1800, then return to avoids unecessary ADC measure.
    // Indeed MSP on => Vcc > 1800
    if (voltage <= 1800) {
        #ifdef ADC_DEBUG
            P1OUT |= MEASURE;
        #endif
        return;
    }

    int threshold = voltage;

    // Configure internal reference
    while (REFCTL0 & REFGENBUSY)
        ;                         // If ref generator busy, WAIT
    REFCTL0 |= REFVSEL_1 | REFON; // Select internal ref
                                  // Internal Reference ON
    while (!(REFCTL0 & REFGENRDY))
        ; // Wait for reference generator

    // First, perform on measure to check if we are already above the threshold
    // Configure ADC12 for one measure
    ADC12CTL0 = ADC12SHT0_2 | ADC12ON;
    ADC12CTL1 = ADC12SHS_0 | ADC12SSEL_2 | ADC12CONSEQ_0 | ADC12SHP;
    ADC12CTL2 = ADC12RES_2;
    ADC12MCTL0 = ADC12INCH_31 | ADC12VRSEL_1;
    ADC12CTL3 = ADC12BATMAP;

    ADC12CTL0 |= ADC12ENC | ADC12SC;
    while ((ADC12CTL1 & ADC12BUSY) != 0)
    {
    }
    ADC12CTL0 &= ~(ADC12ENC | ADC12SC);

    // Check if VCC > threshold. If so, return directly, as we don't need to sleep
    int vread = ADC12MEM0;

    if (vread > threshold)
    {
        ADC12CTL0 = 0; // Turn OFF ADC
        REFCTL0 = 0;
#ifdef ADC_DEBUG
        P1OUT &= ~MEASURE;
#endif
        return;
    }

    // If VCC < threshold, go to sleep until the voltage reaches the threshold

    // Configure ADC12
    // tsample = 16ADC12CLK cycles, tconvert = 14 ADC12CLK cycles
    // software trigger for SOC, ACLK, single ch-single conversion,
    // 2's complement mode
    // tsample controlled by SHT0x settings
    // Channel A31, reference = internal, enable window comparator
    // Set thresholds for ADC12 interrupts
    // Enable Interrupts
    // ADC12CTL0 = ADC12SHT0_2 | ADC12ON;
    ADC12CTL1 = ADC12SHS_1 | ADC12SSEL_1 | ADC12CONSEQ_2 | ADC12SHP;
    // ADC12CTL2 = ADC12RES_2;
    ADC12MCTL0 = ADC12INCH_31 | ADC12VRSEL_1 | ADC12WINC;
    // Link the ADC channel A31 to the AVCC/2 (why ? it has already been setup
    ADC12CTL3 = ADC12BATMAP;
    // Enable conversion
    ADC12HI = threshold; // Set wake-up treshold
    ADC12IER2 = ADC12HIIE;

    // Configure ACLK to source from VLO, with no divider
    // Takes around 1ms between each tick
    CSCTL0_H = CSKEY >> 8; // Unlock CS registers
    // CSCTL2 |= SELA__LFXTCLK | SELS__DCOCLK | SELM__DCOCLK; // set ACLK = XT1; MCLK = DCO
    CSCTL2 |= SELA__VLOCLK;
    CSCTL3 |= DIVA__1; // Set all dividers

    // Configure timer TA0
    P1DIR |= BIT3 | BIT0 | BIT2;
    TA0CTL = TASSEL__ACLK | MC__UP | TACLR | TAIE; // | TAIE;
    TA0R = 0;
    // TA0CCTL0 = CCIE;
    TA0CCTL1 = CCIE | OUTMOD_2;
    TA0CCR0 = 200; // CCR0RESETVAL;
    TA0CCR1 = 1;

    // Start conversion
    ADC12CTL0 |= ADC12ENC;

    __bis_SR_register(LPM3_bits | GIE); // Enter LPM3 w/ interrupts

    // Disable Internal reference
    REFCTL0 = 0;
    // Disable ADC;
    ADC12CTL0 = 0;
    // Disable Timer
    TA0CTL = 0;
#ifdef ADC_DEBUG
    P1OUT &= ~MEASURE;
#endif
}
#else
void sleep_until_voltage_reached(int value)
{
#ifdef ADC_DEBUG
    P1OUT |= MEASURE;
#endif
    if (!adc_init_done)
    {
        adc_init();
    }

    // An error might have occured !
    if (value <= 0)
    {
        value = 3000;
    }

    // Early return if voltage is already reached
    int voltage = adc_measure();

    if (voltage > value && voltage < 4000)
    {
#ifdef ADC_DEBUG
        P1OUT &= ~MEASURE;
#endif
        return;
    }
    // If voltage is not reached, setup an interrupt and enter LPM3
    CSCTL0_H = CSKEY >> 8; // Unlock CS registers
    // CSCTL2 |= SELA__LFXTCLK | SELS__DCOCLK | SELM__DCOCLK; // set ACLK = XT1; MCLK = DCO
    CSCTL2 |= SELA__LFXTCLK;
    CSCTL3 |= DIVA__1; // Set all dividers
    CSCTL4 &= ~LFXTOFF;
    //   do
    //   {
    //     CSCTL5 &= ~LFXTOFFG;                    // Clear XT1 fault flag
    //     SFRIFG1 &= ~OFIFG;
    //   } while (SFRIFG1 & OFIFG);                   // Test oscillator fault flag
    CSCTL0_H = 0; // Lock CS registers

    TA0CTL = TASSEL__ACLK | MC__CONTINUOUS | TACLR | TAIE; // ACLK, contmode, clear TAR
    TA0R = TIMER_INIT;
    __bis_SR_register(LPM3_bits | GIE); // Enter LPM3, enable interrupts
#ifdef ADC_DEBUG
    P1OUT &= ~MEASURE;
#endif
    while (1)
    {
        int voltage = adc_measure();
        if (voltage > value)
        {
            break;
        }
        else
        {
            TA0R = TIMER_INIT;
#ifdef ADC_DEBUG
            P1OUT |= MEASURE;
#endif
            __bis_SR_register(LPM3_bits | GIE); // Enter LPM3, enable interrupts
#ifdef ADC_DEBUG
            P1OUT &= ~MEASURE;
#endif
        }
    }
    // uart_puts("Stopping LFXT...");
    CSCTL0_H = CSKEY >> 8;
    CSCTL4 |= LFXTOFF;
    // uart_puts("[DONE]\nModifying TA0CTL\n");
    TA0CTL = MC__STOP | TACLR;
    // uart_puts("[DONE]\n");
}
#endif

// Timer0_A1 Interrupt Vector (TAIV) handler
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector = TIMER0_A1_VECTOR
__interrupt void TIMER0_A1_ISR(void)
#elif defined(__GNUC__)
void __attribute__((interrupt(TIMER0_A1_VECTOR))) TIMER0_A1_ISR(void)
#else
#error Compiler not supported!
#endif
{
    P1OUT |= BIT0;
    switch (__even_in_range(TA0IV, TA0IV_TAIFG))
    {
    case TA0IV_NONE:
        break; // No interrupt
    case TA0IV_TACCR1:
        break; // CCR1 not used
    case TA0IV_TACCR2:
        break; // CCR2 not used
    case TA0IV_3:
        break; // reserved
    case TA0IV_4:
        break; // reserved
    case TA0IV_5:
        break; // reserved
    case TA0IV_6:
        break;        // reserved
    case TA0IV_TAIFG: // overflow
// P1OUT ^= BIT0;
#ifdef ACTIVE_SAMPLING
                      // __bic_SR_register_on_exit(LPM3_bits|GIE);
// uart_puts("Returning from LPM3 !\n");
#endif
        break;
    default:
        break;
    }
}

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector = ADC12_VECTOR
__interrupt void ADC12_ISR(void)
#elif defined(__GNUC__)
void __attribute__((interrupt(ADC12_VECTOR))) ADC12_ISR(void)
#else
#error Compiler not supported!
#endif
{
    switch (__even_in_range(ADC12IV, ADC12IV_ADC12RDYIFG))
    {
    case ADC12IV_NONE:
        break; // Vector  0:  No interrupt
    case ADC12IV_ADC12OVIFG:
        break; // Vector  2:  ADC12MEMx Overflow
    case ADC12IV_ADC12TOVIFG:
        break;               // Vector  4:  Conversion time overflow
    case ADC12IV_ADC12HIIFG: // Is VCC/2 > threshold ?: High Interrupt
        __bic_SR_register_on_exit(LPM3_bits | GIE);
        break;
    case ADC12IV_ADC12LOIFG:
        break;
    case ADC12IV_ADC12INIFG:
        break;
    case ADC12IV_ADC12IFG0:
        break; // Vector 12:  ADC12MEM0
    case ADC12IV_ADC12IFG1:
        break; // Vector 14:  ADC12MEM1
    case ADC12IV_ADC12IFG2:
        break; // Vector 16:  ADC12MEM2
    case ADC12IV_ADC12IFG3:
        break; // Vector 18:  ADC12MEM3
    case ADC12IV_ADC12IFG4:
        break; // Vector 20:  ADC12MEM4
    case ADC12IV_ADC12IFG5:
        break; // Vector 22:  ADC12MEM5
    case ADC12IV_ADC12IFG6:
        break; // Vector 24:  ADC12MEM6
    case ADC12IV_ADC12IFG7:
        break; // Vector 26:  ADC12MEM7
    case ADC12IV_ADC12IFG8:
        break; // Vector 28:  ADC12MEM8
    case ADC12IV_ADC12IFG9:
        break; // Vector 30:  ADC12MEM9
    case ADC12IV_ADC12IFG10:
        break; // Vector 32:  ADC12MEM10
    case ADC12IV_ADC12IFG11:
        break; // Vector 34:  ADC12MEM11
    case ADC12IV_ADC12IFG12:
        break; // Vector 36:  ADC12MEM12
    case ADC12IV_ADC12IFG13:
        break; // Vector 38:  ADC12MEM13
    case ADC12IV_ADC12IFG14:
        break; // Vector 40:  ADC12MEM14
    case ADC12IV_ADC12IFG15:
        break; // Vector 42:  ADC12MEM15
    case ADC12IV_ADC12IFG16:
        break; // Vector 44:  ADC12MEM16
    case ADC12IV_ADC12IFG17:
        break; // Vector 46:  ADC12MEM17
    case ADC12IV_ADC12IFG18:
        break; // Vector 48:  ADC12MEM18
    case ADC12IV_ADC12IFG19:
        break; // Vector 50:  ADC12MEM19
    case ADC12IV_ADC12IFG20:
        break; // Vector 52:  ADC12MEM20
    case ADC12IV_ADC12IFG21:
        break; // Vector 54:  ADC12MEM21
    case ADC12IV_ADC12IFG22:
        break; // Vector 56:  ADC12MEM22
    case ADC12IV_ADC12IFG23:
        break; // Vector 58:  ADC12MEM23
    case ADC12IV_ADC12IFG24:
        break; // Vector 60:  ADC12MEM24
    case ADC12IV_ADC12IFG25:
        break; // Vector 62:  ADC12MEM25
    case ADC12IV_ADC12IFG26:
        break; // Vector 64:  ADC12MEM26
    case ADC12IV_ADC12IFG27:
        break; // Vector 66:  ADC12MEM27
    case ADC12IV_ADC12IFG28:
        break; // Vector 68:  ADC12MEM28
    case ADC12IV_ADC12IFG29:
        break; // Vector 70:  ADC12MEM29
    case ADC12IV_ADC12IFG30:
        break; // Vector 72:  ADC12MEM30
    case ADC12IV_ADC12IFG31:
        break; // Vector 74:  ADC12MEM31
    case ADC12IV_ADC12RDYIFG:
        break; // Vector 76:  ADC12RDY
    default:
        break;
    }
}
