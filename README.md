# MSP430 libutils

A small utility library for the MSP430FR5969 MCU

It features the following:
- :speaker: a minimal printf
- :floppy_disk: the Mementos¹ checkpoint and restore mechanism, enhanced with customizable wake-up voltage for each checkpoint
- :signal_strength: several methods for querying the ADC, and support for low-power sleeping mechanisms

## Minimal example

```c
#include "platform.h"
#include "uart.h"

void main() {
    platform_init(); // Disable the watchog timer, activate the GPIO and set the frequency to 16MHz
    printf("Hello world !\n");
}
```

To compile a program in the current directory, run the following commands:

1. Compile the library: `make`
2. Compile and link your file: `msp430-elf-gcc -mmcu=msp430fr5969 -o main.elf main.c -L. -lmsp430utils`

## Useful function

- `platform_init()`: Disable the watchog timer, activate the GPIO and set the frequency to 16MHz
- `mementos_boot()`: Called right after main, `mementos_boot` jumps to the previous checkpoint, if any.
- `__mementos_checkpoint(VOLTAGE_VAlUE)`: Checkpoints the current program state, and tell the library to resume execution once the threshold Vin=`VOLTAGE_VALUE`*1.024 mV is reached.
- `sleep_until_voltage_reached(VOLTAGE_VALUE)`: Put the MCU to sleep until the threshold Vin=`VOLTAGE_VALUE`*1.024 mV is reached.

## Settings

If `NOREEXECUTION_POLICY` is defined, the library will, after a checkpoint, sample the ADC and go to sleep if the target voltage is not reached. If `NOREEXECUTION_POLICY` is not defined the library will continue executing the program after a checkpoint, until the platform encounter a power failure.

## Debugging

The library can be compiled with several debug options:

- `ADC_DEBUG`: Defining this variable (with `-DADC_DEBUG`) will enable the ADC debug mode. In this mode, the pin `P1.4` is set to high when the ADC is sampling or sleeping until a given voltage is reached.
- `CHECKPOINT_DEBUG`: Defining this variable (with `-DCHECKPOINT_DEBUG`) will enable the checkpoint debug mode. In this mode, the pin `P1.3` is set to high when the MCU is checkpointing.

By default, the library is compile with `ADC_DEBUG` and `CHECKPOINT_DEBUG` defined.

## Known limitations

- Only work on the MSP430FR5969 MCU
- Only work if compiled with the MSP430-GCC-OPENSOURCE compiler (as it requires parameters to be passed through R12)
- The timer interrupt and the ADC are used by this library, so they cannot be used by the user

[1] Ransford, Benjamin, Jacob Sorber, and Kevin Fu. *"Mementos: System support for long-running computation on RFID-scale devices."* ASPLOS 2011.
