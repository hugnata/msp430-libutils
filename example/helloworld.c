#include "msp430.h"
#include "platform.h"
#include "uart.h"

void main() {
    platform_init(); // Disable the watchog timer, activate the GPIO and set the frequency to 16MHz
    printf("Hello world !\n");
}
