#include "msp430.h"
#include "platform.h"
#include "uart.h"
#include "adc.h"

void main() {
    platform_init(); // Disable the watchog timer, activate the GPIO and set the frequency to 16MHz
    printf("Hello world !\n");
    while(1) {
        sleep_until_voltage_reached(3000);
        printf("Voltage reached 3000v\n");
        __delay_cycles(30000);
    }

}
