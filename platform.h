#ifndef PLATFORM_H
#define PLATFORM_H

/**
 * @brief Setup the platform
 *
 * - Activate the IO
 * - Set the master clock frequency to 16MHz
 * - Disable the watchdog timer
 *
 */
void platform_init(void);

/**
 * @brief Set the IO in a low-power consumption setting
 *
 */
void disable_io(void);

#endif // PLATFORM_H
